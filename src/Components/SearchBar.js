import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import {loginUser} from '../Redux/Actions/userAction';
import { connect } from 'react-redux';

class SearchBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            arraylist: ['rishi', 'sahu', 'mumbai', 'vashi', 'panvel'],
            text: '',
            dropDown: false,
            result: null,
            email: "",
            password: ""
        }
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
      }
    
      handleChange = event => {
          console.log('event.target.value')
        this.setState({
          [event.target.id]: event.target.value
        });
      }
    
      handleSubmit = event => {
        event.preventDefault();
        console.log('username:this.state.email,password:this.state.password',this.state.email,this.state.password)
        this.props.loginUser({username:this.state.email,password:this.state.password})
      }

    searchText = (evt) => {
        evt.preventDefault();
        this.setState({
            text: evt.target.value
        }, () => {
            if (this.state.text.length > 0) {
                let result = this.state.arraylist.filter(list => list.includes(this.state.text, 0))
                console.log('result', this.state.text, result)
                if (result.length > 0) {
                    this.setState({
                        dropDown: true,
                        result: result
                    })
                }
            }
            else {
                this.setState({
                    dropDown: false,
                    result: null
                })
            }
        });

    }


    render() {
        let { result, dropDown } = this.state
        console.log(this.props)
        return (
            <div className="body-container">
                <div className="search-input">
                    <input type="text" onChange={(text) => this.searchText(text)} />
                    {dropDown ?
                        <ul className="searchList">
                            {
                                result.map((suggestion, index) => {
                                    return (
                                        <li key="index" className="listName" onClick={() => alert(suggestion)}>{suggestion}</li>
                                    )
                                })
                            }
                        </ul>
                        : ''}
                </div>
                <div className="Login">
                    <form onSubmit={this.handleSubmit}>
                        <FormGroup controlId="email" bsSize="large">
                            <label>Email</label>
                            <FormControl
                                autoFocus
                                type="email"
                                value={this.state.email}
                                onChange={(e)=>this.handleChange(e)}
                            />
                        </FormGroup>
                        <FormGroup controlId="password" bsSize="large">
                            <label>Password</label>
                            <FormControl
                                value={this.state.password}
                                onChange={(e)=>this.handleChange(e)}
                                type="password"
                            />
                        </FormGroup>
                        <Button
                            block
                            bsSize="large"
                            disabled={false}
                            type="submit"
                        >
                            Login
          </Button>
                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state =>({
    ... state
})

export default  connect(mapStateToProps,{loginUser})(SearchBar)