import React from 'react';
import { persistStore, persistReducer } from 'redux-persist'
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import storage from 'redux-persist/lib/storage';
import { createStore, applyMiddleware, compose  } from 'redux';
import thunk from 'redux-thunk';
import './App.css';
import SearchBar from './Components/SearchBar'
import rootReducer from './Redux/Reducers/RootReducer'

//for persist storage

// const persistConfig = {
//   key: 'root',
//   storage,
//   whitelist: ['insert all the  reduxer elements that you want to store in persist storage'],
// }
// const middleWares = [
//   thunk]
// const persistedReducer = persistReducer(persistConfig, rootReducer) //for persist storage
// const store = createStore(
//   // persistedReducer,	for persist storage
//   compose(
//       applyMiddleware(
//           ...middleWares
//       )
//   )
// )
// const persistor = persistStore(store) //for persist storage


const store = createStore(rootReducer, applyMiddleware(thunk));


function App() {
  return (
    <Provider store={store}>
      {/* <PersistGate loading={null} persistor={persistor}>  */}
    <div className="App">
      <div className="search-bar">
        <div className="search-bar-container">
          <SearchBar/>
        </div>
      </div>
    </div>
    {/* </PersistGate> */}
    </Provider>
  );
}

export default (App);
