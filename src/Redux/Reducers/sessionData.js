const initialState = {
  storeDetail: {},
  sessionInfo: []
};

const sessionDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SELECTED_STORE":
      return { ...state, storeDetail: action.storeInfo };
    case "SESSION_DATA":
      return { ...state, sessionInfo: action.storeInfo };
    default:
      return state;
  }
};

export default sessionDataReducer;
