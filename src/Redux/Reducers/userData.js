const initialState = {
  userData: {},
  isLoading: false,
};

const userData = (state = initialState, action) => {
  switch (action.type) {
    case "USER_DATA":
      return { ...state, userData: action.userData };
    case "SET_LOADING":
      return { ...state, isLoading: action.status };  
    default:
      return state;
  }
};

export default userData;
