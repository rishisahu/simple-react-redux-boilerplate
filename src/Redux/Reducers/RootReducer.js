import { combineReducers } from "redux";
import userData from "./userData";
import sessionDataReducer from "./sessionData";

export default combineReducers({
  userData,
  sessionDataReducer
});
