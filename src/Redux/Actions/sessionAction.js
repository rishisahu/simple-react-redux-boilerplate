import { AsyncStorage } from "react-native";

export function sessionData(payload) {
  return {
    type: "SESSION_DATA",
    sessionInfo: payload
  };
}