import api from '../Api/api'
//v1/login/
export function loginUser(url,data,callback=()=>{}) {

  return dispatch => {
    dispatch(setLoadiing(true))
    api.post(url,data)
    .then((response, error) => {
      dispatch(userLoginData(response));
      dispatch(setLoadiing(false))
      callback()
    }).catch((error)=>{
      dispatch(setLoadiing(false));
    }) 
  }
}

export function setLoadiing (status){
  return {
    status,
    type: "SET_LOADING",
  };
}

export function userLoginData(payload) {
  return {
    type: "USER_DATA",
    userData: payload
  };
}

